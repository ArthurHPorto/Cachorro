//
//  ViewController.swift
//  Cachorro
//
//  Created by Arthur Porto on 22/09/22.
//

import UIKit
import Alamofire
import Kingfisher

struct Dog: Decodable {
    let message: String
    let status: String
}

class ViewController: UIViewController {

    @IBOutlet var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getNewDog()
    }

    @IBAction func reloadImage(_ sender: Any) {
        getNewDog()
    }
    
    func getNewDog() {
        AF.request("https://dog.ceo/api/breeds/image/random").responseDecodable(of: Dog.self){
            response in
            if let dog = response.value {
                self.imageView.kf.setImage(with: URL(string: dog.message))
            }
        }
    }
    
}

